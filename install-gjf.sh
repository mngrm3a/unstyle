#!/usr/bin/env bash

NAME=google-java-format-${1}

curl \
  -L "https://github.com/google/google-java-format/releases/download/${NAME}/${NAME}-all-deps.jar" \
  -o "${NAME}-all-deps.jar"

cat > "${NAME}" << EOF
#!/usr/bin/env bash

exec java -jar ${NAME}-all-deps.jar \$@
EOF

chmod +x "${NAME}"