# unstyle

unstyle is a primitive git filter to undo the arrangement of annotations and the
indentation enforced by the Google Code Style for Java.

## Features

* each annotation is moved to a separate line
* a custom indentation (either tabs or spaces) can be set 

Without specifying a tab width, unstyle will replace any sequence of 2 spaces
with a tab character. If a tab width is specified (`-w <NUMBER>` option), each
occurrence will be replaced with the given number of spaces.

## Example

Piping the following trough `unstyle -w 4`
```java
@Foo @Bar(value="bar")
@Baz(value="baz")
public class Foo{
  @Foo @Bar(value="bar")
  @Baz(value="baz")private Long timeAgo;

  @Foo @Bar(value="bar")
  @Baz(value="baz")private void foo() {
    return;
  }
}
```

will output

```java
@Foo
@Bar(value="bar")
@Baz(value="baz")
public class Foo{
    @Foo
    @Bar(value="bar")
    @Baz(value="baz")
    private Long timeAgo;

    @Foo
    @Bar(value="bar")
    @Baz(value="baz")
    private void foo() {
        return;
    }
}
```

## Build

After building the binary, copy it into a directory in your `$PATH`.

### With go installed

```console
 go get -v gitlab.com/mngrm3a/unstyle
```

### With docker

Set `GOOS` and `GOARCH` according to your operating system and architecture. See
https://golang.org/doc/install/source#environment for valid combinations.
```console
docker run -e GOOS=<OS> -e GOARCH=<ARCH> -v /tmp/go/bin:/go/bin \
    golang go get gitlab.com/mngrm3a/unstyle
```

The resulting binary file is located at `/tmp/go/bin/<arch>/unstyle`.

## Setup

Since filters not configured in `.git/config` or `~/.gitconfig` (and therefore
unknown to git) are silently ignored, it's also save to edit and check in
`.gitattributes`.

For the filters to become active, you may need to delete all files tracked by
git and then check them out again.

### unstyle

[Download](https://github.com/google/google-java-format/releases)
google-java-format-\<verion\>-all-deps.jar . Within the project directory run:

```console
git config --local filter.unstyle.clean 'java -jar path/to/google-java-format-<verion>-all-deps.jar -'
git config --local filter.unstyle.smudge 'unstyle -t 4'
echo '*.java filter=unstyle' >> .git/info/attributes
```

### coreutils (spaces to tabs conversion only)

#### Linux

```console
git config --local filter.s2t.clean 'expand --tabs=2 --initial'
git config --local filter.s2t.smudge 'unexpand --tabs=2 --first-only'
echo '*.java filter=s2t' >> .git/info/attributes
```

#### macOS / BSD

```console
git config --local filter.s2t.clean 'expand -t2'
git config --local filter.s2t.smudge 'unexpand -t2'
echo '*.java filter=s2t' >> .git/info/attributes
```
