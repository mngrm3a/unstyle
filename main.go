///usr/bin/env go run "$0" "$@"; exit "$?"
package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"regexp"
	"strings"
)

const GCS_TAB_WIDTH = 2

var INDENT_RE *regexp.Regexp = regexp.MustCompile("^ +")
var ANNOTATION_RE *regexp.Regexp = regexp.MustCompile("@[[:alnum:]]+(?:\\(.+?\\))?")

func main() {
	// parse command line args
	tabWidth := flag.Int("w", 0, "set tab width")
	flag.Parse()

	// set tab string
	var tab string
	if *tabWidth == 0 {
		tab = "\t"
	} else {
		tab = strings.Repeat(" ", *tabWidth)
	}

	// run scanner loop
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		// indentation prefix
		indent := strings.Repeat(
			tab,
			len(INDENT_RE.FindString(scanner.Text()))/GCS_TAB_WIDTH)

		// wrap lines
		lines := ANNOTATION_RE.FindAllString(scanner.Text(), -1)
		if len(lines) == 0 {
			lines = []string{scanner.Text()}
		} else {
			declarations := ANNOTATION_RE.Split(scanner.Text(), -1)
			numDeclations := len(declarations)
			if numDeclations > 0 && declarations[numDeclations-1] != "" {
				lines = append(
					lines,
					declarations[numDeclations-1])
			}
		}

		// print result
		for _, line := range lines {
			fmt.Printf("%s%s\n", indent, strings.TrimLeft(line, " \t"))
		}
	}
}
