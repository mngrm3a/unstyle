{ pkgs ? import <nixpkgs> { } }:
let
in with pkgs; mkShell { buildInputs = [ go gotools gopkgs go-outline gopls ]; }
