@Foo @Bar(value="bar")
@Baz(value="baz")
public class Foo{
  @Foo @Bar(value="bar")
  @Baz(value="baz")private Long timeAgo;

  @Foo @Bar(value="bar")
  @Baz(value="baz")private void foo() {
    return;
  }
}